package oop.lab1;

//TODO Write class Javadoc
/**
 * A simple model of student that inherits from Person with a name and id
 * @author Arut Thanomwatana
 * @version 2015.01.13 
 */
public class Student extends Person {
	/** the Student's ID.Contain set of Number */
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Person
	 * @param id is the ID of the new Student 
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/**
	 * Compare student's by id.
	 * They are equal if the id matches.
	 * @param obj is another Object to compare to this one.
	 * @return true if the id is same, false otherwise.
	 */
	public boolean equals(Object obj) {
		if(obj==null) return false;
		if(obj.getClass() != this.getClass()) return false;
		Student other = (Student) obj;
		return this.id == other.id;
	}
}
